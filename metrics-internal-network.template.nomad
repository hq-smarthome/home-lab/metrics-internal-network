job "metrics-internal-network" {
  type = "service"
  region = "global"
  datacenters = ["proxima"]

  group "server-rack-1" {
    count = 1

    affinity {
      attribute = "${attr.unique.hostname}"
      value = "server-rack-1"
      weight = 100
    }

    constraint {
      attribute = "${meta.rack}"
      value = "rack-1"
    }

    network {
      mode = "bridge"
    }

    service {
      name = "router-metrics"
      task = "telegraf"

      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "influxdb"
              local_bind_port = 8086
            }
          }
        }
      }
    }

    task "router" {
      driver = "docker"

      vault {
        policies = ["snmp"]
      }

      resources {
        cpu = 1400
        memory = 100
      }

      config {
        image = "[[ .metricsImage ]]"

        auth {
          username = "[[ env "REGISTRY_USERNAME" ]]"
          password = "[[ env "REGISTRY_PASSWORD" ]]"
        }

        volumes = [
          "local/telegraf.conf:/etc/telegraf/telegraf.conf"
        ]
      }

      template {
        data = <<EOH
          {{ with secret "snmp/server-rack-1-router" }}
          TZ='Europe/Stockholm'
          INFLUX_TOKEN="{{ index .Data.data "TELEGRAF_TOKEN" }}"
          SNMP_HOST="{{ index .Data.data "SNMP_HOST" }}"
          SNMP_NODE="server-rack-1-router"
          SNMP_AUTH_USER="{{ index .Data.data "SNMP_AUTH_USER" }}"
          SNMP_AUTH_PROTOCOL="{{ index .Data.data "SNMP_AUTH_PROTOCOL" }}"
          SNMP_AUTH_PASS="{{ index .Data.data "SNMP_PASS" }}"
          SNMP_ENCRYPTION_PROTOCOL="{{ index .Data.data "SNMP_ENCRYPTION_PROTOCOL" }}"
          SNMP_ENCRYPTION_PASS="{{ index .Data.data "SNMP_PASS" }}"
          {{ end }}
        EOH

        destination = "secrets/collector.env"
        env = true
        change_mode = "restart"
      }

      template {
        data = <<EOH
[[ fileContents "./config/telegraf.usg.template.conf" ]]
        EOH

        destination = "local/telegraf.conf"
        change_mode = "noop"
      }
    }

    task "switch" {
      driver = "docker"

      vault {
        policies = ["snmp"]
      }

      resources {
        cpu = 1400
        memory = 100
      }

      config {
        image = "[[ .metricsImage ]]"

        auth {
          username = "[[ env "REGISTRY_USERNAME" ]]"
          password = "[[ env "REGISTRY_PASSWORD" ]]"
        }

        volumes = [
          "local/telegraf.conf:/etc/telegraf/telegraf.conf"
        ]
      }

      template {
        data = <<EOH
          {{ with secret "snmp/server-rack-1-switch" }}
          TZ='Europe/Stockholm'
          INFLUX_TOKEN="{{ index .Data.data "TELEGRAF_TOKEN" }}"
          SNMP_HOST="{{ index .Data.data "SNMP_HOST" }}"
          SNMP_NODE="server-rack-1-switch"
          SNMP_AUTH_USER="{{ index .Data.data "SNMP_AUTH_USER" }}"
          SNMP_AUTH_PROTOCOL="{{ index .Data.data "SNMP_AUTH_PROTOCOL" }}"
          SNMP_AUTH_PASS="{{ index .Data.data "SNMP_PASS" }}"
          SNMP_ENCRYPTION_PROTOCOL="{{ index .Data.data "SNMP_ENCRYPTION_PROTOCOL" }}"
          SNMP_ENCRYPTION_PASS="{{ index .Data.data "SNMP_PASS" }}"
          {{ end }}
        EOH

        destination = "secrets/collector.env"
        env = true
        change_mode = "restart"
      }

      template {
        data = <<EOH
[[ fileContents "./config/telegraf.usw.template.conf" ]]
        EOH

        destination = "local/telegraf.conf"
        change_mode = "noop"
      }
    }

    task "access-point" {
      driver = "docker"

      vault {
        policies = ["snmp"]
      }

      resources {
        cpu = 1400
        memory = 100
      }

      config {
        image = "[[ .metricsImage ]]"

        auth {
          username = "[[ env "REGISTRY_USERNAME" ]]"
          password = "[[ env "REGISTRY_PASSWORD" ]]"
        }

        volumes = [
          "local/telegraf.conf:/etc/telegraf/telegraf.conf"
        ]
      }

      template {
        data = <<EOH
          {{ with secret "snmp/server-rack-1-ap" }}
          TZ='Europe/Stockholm'
          INFLUX_TOKEN="{{ index .Data.data "TELEGRAF_TOKEN" }}"
          SNMP_HOST="{{ index .Data.data "SNMP_HOST" }}"
          SNMP_NODE="server-rack-1-ap"
          SNMP_AUTH_USER="{{ index .Data.data "SNMP_AUTH_USER" }}"
          SNMP_AUTH_PROTOCOL="{{ index .Data.data "SNMP_AUTH_PROTOCOL" }}"
          SNMP_AUTH_PASS="{{ index .Data.data "SNMP_PASS" }}"
          SNMP_ENCRYPTION_PROTOCOL="{{ index .Data.data "SNMP_ENCRYPTION_PROTOCOL" }}"
          SNMP_ENCRYPTION_PASS="{{ index .Data.data "SNMP_PASS" }}"
          {{ end }}
        EOH

        destination = "secrets/collector.env"
        env = true
        change_mode = "restart"
      }

      template {
        data = <<EOH
[[ fileContents "./config/telegraf.uap.template.conf" ]]
        EOH

        destination = "local/telegraf.conf"
        change_mode = "noop"
      }
    }
  }

  group "server-rack-2" {
    count = 1

    affinity {
      attribute = "${attr.unique.hostname}"
      value = "server-rack-2"
      weight = 100
    }

    constraint {
      attribute = "${meta.rack}"
      value = "rack-2"
    }

    network {
      mode = "bridge"
    }

    service {
      name = "router-metrics"
      task = "telegraf"

      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "influxdb"
              local_bind_port = 8086
            }
          }
        }
      }
    }

    task "switch" {
      driver = "docker"

      vault {
        policies = ["snmp"]
      }

      resources {
        cpu = 1400
        memory = 100
      }

      config {
        image = "[[ .metricsImage ]]"

        auth {
          username = "[[ env "REGISTRY_USERNAME" ]]"
          password = "[[ env "REGISTRY_PASSWORD" ]]"
        }

        volumes = [
          "local/telegraf.conf:/etc/telegraf/telegraf.conf"
        ]
      }

      template {
        data = <<EOH
          {{ with secret "snmp/server-rack-2-switch" }}
          TZ='Europe/Stockholm'
          INFLUX_TOKEN="{{ index .Data.data "TELEGRAF_TOKEN" }}"
          SNMP_HOST="{{ index .Data.data "SNMP_HOST" }}"
          SNMP_NODE="server-rack-2-switch"
          SNMP_AUTH_USER="{{ index .Data.data "SNMP_AUTH_USER" }}"
          SNMP_AUTH_PROTOCOL="{{ index .Data.data "SNMP_AUTH_PROTOCOL" }}"
          SNMP_AUTH_PASS="{{ index .Data.data "SNMP_PASS" }}"
          SNMP_ENCRYPTION_PROTOCOL="{{ index .Data.data "SNMP_ENCRYPTION_PROTOCOL" }}"
          SNMP_ENCRYPTION_PASS="{{ index .Data.data "SNMP_PASS" }}"
          {{ end }}
        EOH

        destination = "secrets/collector.env"
        env = true
        change_mode = "restart"
      }

      template {
        data = <<EOH
[[ fileContents "./config/telegraf.usw.template.conf" ]]
        EOH

        destination = "local/telegraf.conf"
        change_mode = "noop"
      }
    }

    task "access-point" {
      driver = "docker"

      vault {
        policies = ["snmp"]
      }

      resources {
        cpu = 1400
        memory = 100
      }

      config {
        image = "[[ .metricsImage ]]"

        auth {
          username = "[[ env "REGISTRY_USERNAME" ]]"
          password = "[[ env "REGISTRY_PASSWORD" ]]"
        }

        volumes = [
          "local/telegraf.conf:/etc/telegraf/telegraf.conf"
        ]
      }

      template {
        data = <<EOH
          {{ with secret "snmp/server-rack-2-ap" }}
          TZ='Europe/Stockholm'
          INFLUX_TOKEN="{{ index .Data.data "TELEGRAF_TOKEN" }}"
          SNMP_HOST="{{ index .Data.data "SNMP_HOST" }}"
          SNMP_NODE="server-rack-2-ap"
          SNMP_AUTH_USER="{{ index .Data.data "SNMP_AUTH_USER" }}"
          SNMP_AUTH_PROTOCOL="{{ index .Data.data "SNMP_AUTH_PROTOCOL" }}"
          SNMP_AUTH_PASS="{{ index .Data.data "SNMP_PASS" }}"
          SNMP_ENCRYPTION_PROTOCOL="{{ index .Data.data "SNMP_ENCRYPTION_PROTOCOL" }}"
          SNMP_ENCRYPTION_PASS="{{ index .Data.data "SNMP_PASS" }}"
          {{ end }}
        EOH

        destination = "secrets/collector.env"
        env = true
        change_mode = "restart"
      }

      template {
        data = <<EOH
[[ fileContents "./config/telegraf.uap.template.conf" ]]
        EOH

        destination = "local/telegraf.conf"
        change_mode = "noop"
      }
    }
  }
}
